const companyDetail = require('./company_detail.json');
const companyList = require('./company_list.json');
const aoList = require('./ao_list.json');

module.exports = () => ({
  company_detail: companyDetail,
  company_list: companyList,
  ao_list: aoList
});
